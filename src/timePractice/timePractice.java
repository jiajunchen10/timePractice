package timePractice;

import java.sql.Time;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.function.Consumer;

public class timePractice {

	public static void main(String[] args) {
		Instant begin = Instant.now();
		
		ArrayList<Object> list = new ArrayList<>();
		LocalDate today = LocalDate.now();
		list.add(today);
		LocalTime now = LocalTime.now();
		list.add(now);
		LocalDateTime thisMoment = LocalDateTime.of(today, now);
		list.add(thisMoment);
		ZonedDateTime bostonTime = ZonedDateTime.now();
		list.add(bostonTime);
		list.forEach(System.out::println);
		
		System.out.println(ZonedDateTime.of(today,now, ZoneId.of("Asia/Shanghai")));
		
		System.out.println("printing the list in HH/MM/DD/MM/YY format: ");
		DateTimeFormatter format = DateTimeFormatter.ofPattern("HHMM, dd MMM yyyy");
		System.out.println(format.format(thisMoment));
		
		Period tenD = Period.ofDays(10);
		today.plus(tenD);
		System.out.println("10 days after today is: "+today);
		
		Duration oneH = Duration.ofHours(1);
		now.plus(oneH);
		System.out.println("1 hour after now is: "+ now);
		
		System.out.println("====================================================");
		LocalDate myBD = LocalDate.of(1995, Month.AUGUST, 12);
		System.out.println("I am exactly "+Period.between(today, myBD)+" old");
		Period difference = Period.between(myBD, today);
		System.out.println("My birthday is comming in "+difference.getMonths()+" months & "+difference.getDays()+" days, such a long time, i can't wait");
		Duration dif = Duration.ofMinutes(difference.getDays()*24*60);
		System.out.println("that is "+dif.toMinutes()+" minutes, such a huge number!");
		Instant end = Instant.now();
		Duration runtime = Duration.between(begin, end);
		System.out.println("this program takes "+runtime.toMillis()+" mili-seconds to run");
	}

}
